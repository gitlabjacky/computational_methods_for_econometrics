using XLSX, DataFrames, DataFramesMeta, FLoops, Optim,
      Distributions, Random, StatsFuns, LinearAlgebra, ForwardDiff, 
      HypothesisTests, PrettyTables


function read_data(path="../CilibertoTamerEconometrica.xlsx")
    df = DataFrame(XLSX.readtable(path, "Sheet1"))

    _X = @select df :marketsize :marketdistance 
    X = Matrix{Float64}(_X)
    X = hcat(ones(size(X, 1)), X)  # dim: number_of_market * 3

    
    Z = hcat(  # dim: (number_of_market*6) * 2
        reduce(
            vcat, 
            [    
                Vector{Float64}(i)
                for i=eachrow(@select(
                    df,
                    :marketpresenceAA,
                    :marketpresenceDL,
                    :marketpresenceUA,
                    :marketpresenceAL,
                    :marketpresenceLCC,
                    :marketpresenceWN,
                ))
            ]
        ),
        reduce(
            vcat,
            [
                Vector{Float64}(i)
                for i=eachrow(@select(
                    df,  
                    :mindistancefromhubAA,
                    :mindistancefromhubDL,
                    :mindistancefromhubUA,
                    :mindistancefromhubAL,
                    :mindistancefromhubLCC,
                    :mindistancefromhubWN
                ))
            ]
        )
    )
    Z = Matrix{Float64}(Z)

    _N = @select df :airlineAA :airlineDL :airlineUA :airlineAL :airlineLCC :airlineWN
    N = sum(Matrix{Int64}(_N), dims=2)    # dim: number_of_market * 1

    I = reduce(  # dim: (number_of_market*6) * 2
        vcat,
        [
            Vector{Int64}(i)
            for i=eachrow(_N)
        ]
    )

    return X, Z, N, I
end


function output_table(_paramnames, _coevec, obj_fn=nothing, nofobs=nothing; format=:text, test=true)
    coevec = reduce(vcat, _coevec)
    paramnames = reduce(vcat, [
        vcat(_paramnames[i], repeat([Symbol("")], length(_coevec[i])-1))
        for i = eachindex(_paramnames, _coevec)
    ])

    if test
        hessian = ForwardDiff.hessian(obj_fn, coevec)
        diagonal = diag(inv(hessian))

        stderr = sqrt.(diagonal)
        t_stats = coevec ./ stderr
        p_value = [pvalue(TDist(nofobs - length(coevec)), i; tail=:both) for i in t_stats]
        tt = cquantile(Normal(0,1), 0.025)
        cilow = [round(coevec[i]-tt*stderr[i], digits=5) for i = eachindex(coevec)]
        ciup = [round(coevec[i]+tt*stderr[i], digits=5) for i = eachindex(coevec)]



        data = hcat(paramnames, coevec, stderr, t_stats, p_value, cilow, ciup)
        header = ["Var.", "Coef.", "Std. Err.", "z", "Pr(>|z|)", "Lower 95%", "Upper 95%"]
        formatters = ft_printf("%5.4f", 2:8)
    else
        data = hcat(paramnames, coevec)
        header = ["Var.", "Coef."]
        formatters = ft_printf("%5.4f", 2:2)
    end


    pretty_table(
        data,
        header=header,
        formatters=formatters,
        compact_printing=true,
        backend=Val(format)
    )
end


function ordered_probit_log_likelihood(β, δ ; X, N, Kᵢ=6)
    @floop for i = eachindex(N)
        Nᵢ = N[i]
        if Nᵢ == 0
            likelihoodᵢ = normcdf(-X[i, :]'*β)
        elseif Nᵢ == Kᵢ
            likelihoodᵢ = 1 - normcdf(-X[i, :]'*β+δ*log(Kᵢ))
        else
            likelihoodᵢ = normcdf(-X[i, :]'*β+δ*log(Nᵢ+1)) - normcdf(-X[i, :]'*β+δ*log(Nᵢ))
        end

        @reduce llh += log(likelihoodᵢ)
    end

    return llh
end


struct GMMResult{T}
    res::T
end

function minimizer(res::GMMResult; k)
    θ = Optim.minimizer(res.res)
    β, α, δ, _ρ = θ[begin:begin+k-1], θ[begin+k:end-2], θ[end-1],  θ[end]
    ρ = tanh(_ρ) # [-Inf, Inf] -> [-1, 1]
    σ = sqrt(1-ρ^2)

    return β, α, δ, ρ, σ
end
minimizer(res::Optim.OptimizationResults) = Optim.minimizer(res)

minimum(res::GMMResult) = Optim.minimum(res.res)
minimum(res::Optim.OptimizationResults) = Optim.minimum(res)


function simulation(β, α, δ, ρ, σ, Xᵢ, Zᵢ, Kᵢ=6)
    uᵢₜ = rand(Normal(0, 1), Kᵢ+1)

    _n̂ₜ = 0:Kᵢ
    πᵢₜ = [
        Xᵢ'*β .+ Zᵢ*α .- δ*log(n) .+ ρ*uᵢₜ[begin] + σ*uᵢₜ[begin+1:end]
        for n in _n̂ₜ
    ]
    idx = maximum(findall(  # equilibrium condition for nₜ
            x -> sum(x[1] .>= 0) >= x[2], 
            collect(zip(πᵢₜ, _n̂ₜ))
    ))
    

    P̂ₜ = map(x->x>=0 ? one(Int64) : zero(Int64), πᵢₜ[idx])
    n̂ₜ = _n̂ₜ[idx]

    return n̂ₜ , P̂ₜ
end

function moment_condition(β, α, δ, _ρ, X, Z, N, I; T, Kᵢ=6, seed=20230508)
    Random.seed!(seed)
    ρ = tanh(_ρ)  # [-Inf, Inf] -> [-1, 1]
    σ = sqrt(1-ρ^2)
   
    g = Matrix{Float64}(undef, 33, length(N))
    start = 1
    for i = eachindex(N)  
        terminate = start + Kᵢ - 1

        Zᵢ = Z[start:terminate, :]  # dim_of_Z: (number_of_market*6) * 2
        Xᵢ = X[i, :]  # dim_of_X: number_of_market * 3

        n̂, P̂ = Vector{Int64}(undef, T), Matrix{Int64}(undef, Kᵢ, T)
        @floop for t = eachindex(n̂)
            n̂[t], P̂[:, t] = simulation(β, α, δ, ρ, σ, Xᵢ, Zᵢ)
        end
        
        # construct the moment condition
        vᵢ₀ = N[i] - mean(n̂)
        vᵢ = (I[start:terminate] .- mean(P̂, dims=2))[:, 1]

        
        g[begin:30, i] = vec(
            vᵢ .* 
            hcat(repeat(reshape(Xᵢ, 1, 3), outer=(6, 1)), Zᵢ)
        )
        g[31:end, i] = vᵢ₀ * Xᵢ

        start = terminate + 1
    end

    ḡ = mean(g, dims=2)[:, 1]
    return ḡ
end


function GMM_estimation(init, A, X, Z, N, I; 
                        T=200, 
                        solver=:NelderMead, 
                        tolerance=1e-8, 
                        maxit=2000, 
                        verbose=false
                        )
    k = size(X, 2)
    function obj_fn(θ)
        ḡ = moment_condition(
            θ[begin:begin+k-1],  # β
            θ[begin+k:end-2],    # α 
            θ[end-1],            # δ
            θ[end],              # ρ 
            X, Z, N, I; T=T
        )

        return ḡ' * A * ḡ
    end
    res = GMMResult(numeric_optimization(
        obj_fn; 
        init=init, 
        solver=solver, 
        tolerance=tolerance, 
        maxit=maxit, 
        verbose=verbose
    ))

    return res
end


function optimal_A_matrix(β, α, δ, ρ, X, Z, N, I; T=200)
    ḡ = moment_condition(β, α, δ, ρ, X, Z, N, I, T=T)
    A = ḡ * ḡ'

    return A
end


function numeric_optimization(obj_fn; init, solver=:NelderMead, tolerance=1e-8, maxit=2000, verbose=true)
    opt = optimize(
        obj_fn, 
        init,
        eval(solver)(), 
        Optim.Options(
            g_tol=tolerance, 
            iterations=maxit, 
            store_trace=verbose, 
            show_trace=verbose
        )
    )
    
    return opt
end


function mixednormal_simulation(;μ₁, μ₂, σ₁, σ₂, N₁, N₂, seed=20220429)
    Random.seed!(seed)
    _x1, _x2 = rand(Normal(μ₁, σ₁), N₁), rand(Normal(μ₂, σ₂), N₂)
    _x = reduce(vcat, (_x1, _x2))
    x = _x[sample(1:(N₁+N₂), N₁+N₂, replace=false)]

    return x
end


"""
caluclate the conditional probability to which component each observation belongs to
"""
cluster1_pmf(x; π₁, π₂, μ₁, μ₂, σ₁, σ₂) = (π₁*normpdf(μ₁, σ₁, x)) / (π₁*normpdf(μ₁, σ₁, x)+π₂*normpdf(μ₂, σ₂, x))
cluster2_pmf(x; π₁, π₂, μ₁, μ₂, σ₁, σ₂) = (π₂*normpdf(μ₂, σ₂, x)) / (π₁*normpdf(μ₁, σ₁, x)+π₂*normpdf(μ₂, σ₂, x))

"""
- Arguments and Keyword Arguments
    - x::Vector{<:Real}: observation of the mixed Gaussian model
    - tolerance::Float64: the convergence criterion
    - maxit::Int64: maximum number of iterations
    - verbose::Bool: wheter to print out the stage of iterations
    - remained: initial guess of parameters
"""
function mixednormal_EM(x; tolerance=1e-8, maxit=2000, verbose=true, π₁=0.5, π₂=0.5, μ₁=28., μ₂=12., σ₁=1., σ₂=1.)
    tol = 1.
    it = 0
    while tol >= tolerance && it <= maxit
        _π₁, _π₂, _μ₁, _μ₂, _σ₁, _σ₂ = π₁, π₂, μ₁, μ₂, σ₁, σ₂
        
        # E step
        pmf1 = cluster1_pmf.(x; π₁=_π₁, π₂=_π₂, μ₁=_μ₁, μ₂=_μ₂, σ₁=_σ₁, σ₂=_σ₂)
        pmf2 = cluster2_pmf.(x; π₁=_π₁, π₂=_π₂, μ₁=_μ₁, μ₂=_μ₂, σ₁=_σ₁, σ₂=_σ₂)
        sum_pmf1, sum_pmf2 = sum(pmf1), sum(pmf2)
        
        # M step
        π₁, π₂ = mean(pmf1), mean(pmf2)
        μ₁, μ₂= dot(x, pmf1)/sum_pmf1, dot(x, pmf2)/sum_pmf2
        σ₁, σ₂= sqrt(dot((x.-_μ₁).^2, pmf1)/sum_pmf1), sqrt(dot((x.-_μ₂).^2, pmf2)/sum_pmf2)

        tol = maximum(
            abs.([π₁, π₂, μ₁, μ₂, σ₁, σ₂] - [_π₁, _π₂, _μ₁, _μ₂, _σ₁, _σ₂])
        )
        it += 1
        verbose && it % 50 == 0. && print("the $(it)th iterations...")
    end

    return π₁, π₂, μ₁, μ₂, σ₁, σ₂
end