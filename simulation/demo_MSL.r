
# Model is  y = theta + u + e
# where  theta is a scalar parameter equal to 3
#        u is extreme value type 1
#        e is N(0,1)

set.seed(10101)

setwd("C:/Users/ssunr/Dropbox/teaching_NTU/Econ7218/R_code")

numobs <- 500
u <- -log(-log(runif(numobs)))
e <- rnorm(numobs,0,1)
y <- 3+u+e

simreps <- 1000

denssim <- function(theta) {
    loglik <- sum(sapply(y, function(y) log(mean((1/sqrt(2 * pi)) * exp(-(y - theta + log(-log(runif(simreps))))^2/2)))))
    return(-loglik)
}

system.time(res <- nlm(denssim, 1.0)) 