using 
    Random, Distributions, Statistics, Optim, 
    XLSX, DataFrames, DataFramesMeta, GLM


# question 1
"""
CDF of U
"""
ucdf(x) = exp(-exp(-x))

"""
invrese CDF of U
"""
inv_ucdf(x) = -log(-log(x))

"""
Simulate the X, y
"""
function generate_data(β₁=0.5, β₂=-0.5, N=400) 
    error = inv_ucdf.(rand(N, 2))
    U₁, U₂ = error[:, 1], error[:, 2]
    X₁, X₂ = randn(N), rand(Chisq(1), N)
    y = [
        β₁*X₁[i]+U₁[i]>β₂*X₂[i]+U₂[i] ? 1 : 0
        for i in eachindex(X₁, X₂, U₁, U₂)
    ]

    return y, X₁, X₂
end


"""
    gridestimation(obj_fn::Function; gridnum::Int64, h::Function, numofparam::Int64, maximize::Bool)

optimize parameters using grid search
- Arguments:
    - `obj_fn::funciton`: the objective function to apply the grid search
    - `gridnum::Int`: number of grid, notice that the grid will be generated with equal length given the interval
    - `h`: non-linear function, default to `f(x)=x` which do not apply the non-linear transformatin
    - `numofparam::Int`: number of parameters
    - `maximize::Bool`: whether it's a maximization problem of minimization, default to `true`
"""
function gridestimation(obj_fn; gridnum, h, numofparam=2, maximize=true)
    gridrange = [range(-5, 5, length=gridnum) for _ = 1:numofparam]
    gridspace = collect(Iterators.product(gridrange...))
    res = [obj_fn(h.(i)) for i in gridspace]
    _, idx = maximize ? findmax(res) : findmin(res)
    optim_param = gridspace[idx]

    return optim_param
end

"""
    numeric_estimation(obj_fn::Function; init, solver::Optim.AbstractOptimizer, tolerance::Real, maxit::Int64, verbose::Bool)

optimize parameters using numeric method through the packages `Optim`
- Arguments
    - `obj_fn::funciton`: the objective function to apply the grid search
    - `init::Vector{<:Real}`: initial condition for `numeric_estimation`
    - `solver::AbstractOptimizer`: numeric optimization algorithm provided by julia package Optim, default to NelderMead
    - `tolerance::AbstractFloat`: optimization will stop when the improvement of the objective function less than `tolerance`
    - `maxit::Int`: optimization will stop when the number of iterations exceed `maxit`
    - `verbose::Bool`: whether to print out the process of numeric optimization, default to `false`
"""
function numeric_estimation(obj_fn; init, solver, tolerance, maxit, verbose)
    opt = optimize(
        obj_fn, 
        init,
        solver, 
        Optim.Options(
            g_tol=tolerance, 
            iterations=maxit, 
            store_trace=verbose, 
            show_trace=verbose
        )
    )
    res = Optim.minimizer(opt)
    return res
end

"""
log PDF of y
"""
logypdf(y, X₁, X₂, β₁, β₂) = y*((β₁*X₁-β₂*X₂)-log(1+exp(β₁*X₁-β₂*X₂))) - (1-y)*log(1+exp(β₁*X₁-β₂*X₂))

"""
General Interface of the function and creat  the type through `s` to implement multiple dispatch
"""
problem1_estimation(s::Symbol; kwargs...) = problem1_estimation(Val{s}(); kwargs...)

"""
- Keyword Arguments:
    - `gridnum::Int`: number of grid, notice that the grid will be generated with equal length given the interval
    - `h::Function`: nonlinear function, default to `f(x)=x`
    - `seed::Real`: random seed to ensure reproducibility
"""
function problem1_estimation(::Val{:grid}; gridnum=100, h=x->x, seed=20230306)
    Random.seed!(seed)
    y, X₁, X₂ = generate_data()
    loglikelihood = β -> sum([
        logypdf(yᵢ, X₁ᵢ, X₂ᵢ, β[1], β[2])  
        for (yᵢ, X₁ᵢ, X₂ᵢ) in zip(y, X₁, X₂)
    ])
    optimβ₁, optimβ₂ = gridestimation(loglikelihood; gridnum=gridnum, h=h)

    return optimβ₁, optimβ₂
end

"""
- Keyword Arguments:
    - `init::Vector{<:Real}`: initial condition for `numeric_estimation`
    - `solver::AbstractOptimizer`: numeric optimization algorithm provided by julia package Optim, default to NelderMead
    - `tolerance::AbstractFloat`: optimization will stop when the improvement of the objective function less than `tolerance`
    - `maxit::Int`: optimization will stop when the number of iterations exceed `maxit`
    - `verbose::Bool`: whether to print out the process of numeric optimization, default to `false`
    - `seed::Real`: random seed to ensure reproducibility
"""
function problem1_estimation(::Val{:numeric};
                             R=100, 
                             init=ones(2), 
                             solver=NelderMead(), 
                             tolerance=1e-8,
                             maxit=2000,
                             verbose=false,
                             seed=20230306)
        Random.seed!(seed)
        optimβ₁, optimβ₂ = zeros(R), zeros(R)
        for i in 1:R
            y, X₁, X₂ = generate_data()
            # since optim do the minimization, - is necessary
            loglikelihood = β -> -sum([
                logypdf(yᵢ, X₁ᵢ, X₂ᵢ, β[1], β[2])  
                for (yᵢ, X₁ᵢ, X₂ᵢ) in zip(y, X₁, X₂)
            ])
            optimβ₁[i], optimβ₂[i] = numeric_estimation(
                loglikelihood,
                init=init,
                solver=solver,
                tolerance=tolerance, 
                maxit=maxit,
                verbose=verbose
            )
        end
        
    return optimβ₁, optimβ₂
end

# question 2
"""
Create the data
"""
function getdata(path="cps09mar.xlsx")
    df = DataFrame(XLSX.readtable(path, "Sheet1"))
    samples = @chain df begin
        @subset(
            :race .== 2,  # select the race blace
            :female .== 1,  # select the female
            :region .== 2,  # select the perple from Midwest
        )
        @transform(
            :age² = :age .^ 2,
            :married = map(
                x -> x in (1,2,3) ? 1 : 0, 
                :marital
            )
        )
        @select(:married, :age, :age², :education)
    end

    res = reduce(hcat, (Matrix{Int64}(samples), ones(Int64, nrow(samples))))
    
    return res
end

"""
fit the logistic model
"""
function logit_estimation(;samples)
    logit = glm(samples[:, 2:end], samples[:, 1], Binomial(), LogitLink())

    return logit
end

"""
- Arguments:
    - `B::Int`: number of bootstrap procedure, default to 400
    - `rng::AbstractRNG`: random number generator, default to `Random.MersenneTwister(1234)`
    - `samples::Matrix{<:Real}`: data to be fit
"""
function bootstrap_coef(B=400, rng=Random.MersenneTwister(1234); samples=getdata())
    nofobs = size(samples, 1)
    coefs = Vector{Vector{Float64}}(undef, B)
    for i = 1:B
        selected_row = sample(rng, 1:nofobs, nofobs, replace=true)
        bootstrap_samples = samples[selected_row, :]
        coefs[i] = coef(logit_estimation(samples=bootstrap_samples))
    end

    return coefs
end

"""
gradient of the `θ`
"""
gradient(β₁, β₂) = [-1/(2*β₂), β₁/(2β₂^2)]

function deltamethod()
    logit = logit_estimation(samples=getdata())
    # vcov is the variance covariance matrix
    coefficient, varmat = coef(logit), vcov(logit)
    grad = gradient(coefficient[1], coefficient[2])
    variance = transpose(grad) * varmat[begin:2, begin:2] * grad

    return variance
end