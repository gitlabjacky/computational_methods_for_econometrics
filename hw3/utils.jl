using CSV, DataFrames, LinearAlgebra, FLoops, Optim


function read_data(group_dir="../group", network_dir="../network")
    y = Vector{Vector{Float64}}(undef, 76)
    X = Vector{Matrix{Float64}}(undef, 76)
    W = Vector{Matrix{Float64}}(undef, 76)
    for g = 1:76
        group_g = DataFrame(CSV.File("$(group_dir)/group$g.dat"))
        group_g.constant .= 1.
        network_g = DataFrame(CSV.File("$(network_dir)/network$g.dat"))

        col = [1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 20]
        y[g] = Vector{Float64}(group_g[:, 18])
        X[g] = Matrix{Float64}(group_g[:, col])
        W[g] = Matrix{Float64}(network_g)
    end
    return y, X, W
end


function epsilon_g(λ, β₁, β₂, α_g, y_g, X_g, W_g)
    n_g = size(W_g, 1)
    I = Matrix{Float64}(LinearAlgebra.I, n_g, n_g)
    ϵ_g = (I - λ*W_g)*y_g - X_g*β₁ - W_g*X_g*β₂ .- α_g

    return ϵ_g
end


function log_likelihood(λ, β₁, β₂, _σ², α, y, X, W)
    σ² = exp(_σ²)
    # σ²  = exp(_σ²) to ensure positivity, then log(exp(_σ²)) = _σ²
    @floop for g = 1:76
        n_g = size(W[g], 1)
        I = Matrix{Float64}(LinearAlgebra.I, n_g, n_g)
        ϵ_g = epsilon_g(λ, β₁, β₂, α[g], y[g], X[g], W[g])

        llh_g = (-n_g/2)*log(2π) - (n_g/2)*_σ² + log(det(I-λ*W[g])) -
                (1/(2σ²))*dot(ϵ_g, ϵ_g)
        @reduce llh += llh_g
    end
    return llh
end


function numeric_optimization(obj_fn; init, solver=:Newton, tolerance=1e-8, maxit=2000, verbose=true)
    opt = optimize(
        obj_fn, 
        init,
        eval(solver)(), 
        Optim.Options(
            g_tol=tolerance, 
            iterations=maxit, 
            store_trace=verbose, 
            show_trace=verbose
        )
    )
    
    return opt
end

minimizer(res) = Optim.minimizer(res)